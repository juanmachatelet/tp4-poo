import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;
import java.io.*;
/**
 * Clase ejecutable para grabar empleados en un archivo.dat
 *
 * @author Juan Chatelet
 * @version 1.0
 */
public class Empresa{
    public static void main(String [] args){
        Scanner teclado = new Scanner(System.in);
        // Inicio del try catch para manejar excepciones
        try {
            // Creando un objeto de tipo archivo secuencial para grabar
            FileOutputStream archiFOS = new FileOutputStream("/home/juanchate/archivosPOO/Empleado.dat", true);
            DataOutputStream archiDOS = new DataOutputStream(archiFOS);
            System.out.println("1-Crear Empleados\nCualquier otra tecla-Terminar Programa");
            int varControl = teclado.nextInt();
            // Grabando archivo
            while (varControl == 1) {
                //Se piden los datos al usuario
                System.out.println("Ingrese el cuil del empleado: ");
                long cuilInput = teclado.nextLong();
                System.out.println("Ingrese el apellido del empleado: ");
                String apellidoInput = teclado.next();
                System.out.println("Ingrese el nombre del empleado: ");
                String nombreInput = teclado.next();
                System.out.println("Ingrese el sueldo del empleado: ");
                double sueldoInput = teclado.nextFloat();
                System.out.println("Ingrese el dia de ingreso del empleado: ");
                int diaInput = teclado.nextInt();
                System.out.println("Ingrese el mes de ingreso del empleado: ");
                int mesInput = teclado.nextInt();
                System.out.println("Ingrese el anio de ingreso del empleado: ");
                int anioInput = teclado.nextInt();
                // Se instancia un calendario con los valores solicitados de la fecha
                Calendar fechaIngreso = new GregorianCalendar(anioInput, mesInput, diaInput);
                // Se instancia un empleado con los datos solicitados
                Empleado inputEmpleado = new Empleado(cuilInput,apellidoInput,nombreInput,sueldoInput, fechaIngreso);
                // Se calcula el sueldo neto
                double sueldoNeto = inputEmpleado.sueldoNeto();
                //Se graban los datos en el archivo Empleado.dat
                archiDOS.writeLong(cuilInput);
                archiDOS.writeUTF(apellidoInput);
                archiDOS.writeUTF(nombreInput);
                archiDOS.writeDouble(sueldoInput);
                archiDOS.writeDouble(sueldoNeto);
                archiDOS.writeInt(inputEmpleado.getFechaIngreso().get(Calendar.DAY_OF_MONTH));
                archiDOS.writeInt(inputEmpleado.getFechaIngreso().get(Calendar.MONTH));
                archiDOS.writeInt(inputEmpleado.getFechaIngreso().get(Calendar.YEAR));
                // Se le pregunta al usuario si quiere seguir agregando empleados
                System.out.println("Quiere seguir creando empleados?");
                varControl = teclado.nextInt();
            }
            // Se cierra el Scanner
            teclado.close();
            // Se cierra el sistema de archivos logico
            archiDOS.close();
        } // Cierra try 
        catch(FileNotFoundException fnfe) {
            System.out.println("Archivo no encontrado");
        }
        catch(IOException ioe){
            System.out.println("Error al grabar");
        }       
    } // Cierra main
} // Cierra clase
