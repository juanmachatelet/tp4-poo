
/**
 * Abstraccion del concepto Empleado
 *
 * @author Juan Chatelet
 * @version 1.0
 */

import java.util.Date;
import java.util.*;
 
public class Empleado{
    private long cuil;
    private String apellido;
    private String nombre;
    private double sueldoBasico;
    private Calendar fechaIngreso;


    /**
     * Constructor de Empleado
     * @param p_cuil Cuil del Empleado
     * @param p_apellido Apellido del Empleado
     * @param p_nombre Nombre del Empleado
     * @param p_sueldoBasico Sueldo Basico del Empleado
     * @param p_anioIngreso Anio de ingreso del Empleado
     */
    public Empleado(long p_cuil, String p_apellido, String p_nombre, double p_sueldoBasico, int p_anioIngreso){
        this.setCuil(p_cuil);
        this.setApellido(p_apellido);
        this.setNombre(p_nombre);
        this.setSueldoBasico(p_sueldoBasico);
        this.setAnioIngreso(p_anioIngreso);
    }

    /**
     * Constructor de Empleado pasando un Calendar como parametro
     * @param p_cuil
     * @param p_apellido
     * @param p_nombre
     * @param p_sueldoBasico
     * @param p_fechaIngreso
     */
    public Empleado(long p_cuil, String p_apellido, String p_nombre, double p_sueldoBasico, Calendar p_fechaIngreso){
        this.setCuil(p_cuil);
        this.setApellido(p_apellido);
        this.setNombre(p_nombre);
        this.setSueldoBasico(p_sueldoBasico);
        this.setFechaIngreso(p_fechaIngreso);
    }


    /**
     * Metodo para calcular la antiguedad de un empleado
     * @return Antiguedad del empleado
     */
    public int antiguedad(){
        Calendar fechaHoy = new GregorianCalendar();
        int anioHoy = fechaHoy.get(Calendar.YEAR);
        return(anioHoy - getAnioIngreso());
        //Date date = new Date();
        //int fecha = date.getYear();
        //return(2019 - getAnioIngreso());
    }

    /**
     * Metodo que verifica si el empleado ha cumplido
     * 1 anio mas en la empresa
     * @return true si es su aniversario de trabajo, false en caso contrario
     */
    public boolean esAniversario(){
        Calendar fechaHoy = new GregorianCalendar();
        int mesHoy = fechaHoy.get(Calendar.MONTH)+1;
        int diaHoy = fechaHoy.get(Calendar.DATE);
        int mesIngreso = this.getFechaIngreso().get(Calendar.MONTH)+1;
        int diaIngreso = this.getFechaIngreso().get(Calendar.DAY_OF_MONTH);
        if ((diaIngreso == diaHoy) && (mesIngreso == mesHoy)){
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * Metodo para calcular el bono por antiguedad
     * @return Bono por antiguedad
     */
    private double adicional(){
        if(antiguedad() < 2){
            return (2 * this.getSueldoBasico() / 100);
        } else if(antiguedad() >= 2 && antiguedad() < 10){
            return (4 * this.getSueldoBasico() / 100);
        } else {
            return (6 * this.getSueldoBasico() / 100);
        }
    }


    /**
     * Metodo para calcular el descuento del sueldo
     * @return Descuento del sueldo
     */
    private double descuento(){
        return(2 * this.getSueldoBasico() / 100 + 12);
    }


    /**
     * Metodo para calcular el sueldo neto
     * @return Sueldo Neto
     */   
    public double sueldoNeto(){
        return(this.getSueldoBasico() + this.adicional() - this.descuento()); // Al sueldo basico se le suma el adicional y se le resta el descuento
    }

    /**
     * Metodo para devolver una concatenacion del nombre y el apellido de la persona
     * @return Nombre + Apellido
     */
    public String nomYApe(){
        return(this.getNombre() + " " + this.getApellido());
    }
    
    /**
     * Metodo para devolver una concatenacion del apellido y el nombre de la persona
     * @return Apellido + Nombre
     */
    public String apeYNom(){
        return(this.getApellido() + " " + this.getNombre());
    }

    public void mostrar(){
        System.out.println("Nombre y apellido: " + this.getNombre());
        System.out.println("CUIL: " + this.getCuil() + "\t" + "Antiguedad: " + this.antiguedad() + " anios de servicio");
        System.out.println("Sueldo Neto: $" + this.sueldoNeto());
    }

    public String mostrarLinea(){
        return(cuil + "\t" + this.getApellido() + "," + this.getNombre() + " ..............." + "$" + this.sueldoNeto());
    }

    public long getCuil() {
        return this.cuil;
    }

    private void setCuil(long p_cuil) {
        this.cuil = p_cuil;
    }

    public String getApellido() {
        return this.apellido;
    }

    private void setApellido(String p_apellido) {
        this.apellido = p_apellido;
    }

    public String getNombre() {
        return this.nombre;
    }

    private void setNombre(String p_nombre) {
        this.nombre = p_nombre;
    }

    public double getSueldoBasico() {
        return this.sueldoBasico;
    }

    private void setSueldoBasico(double p_sueldoBasico) {
        this.sueldoBasico = p_sueldoBasico;
    }

    public int getAnioIngreso() {
        return this.fechaIngreso.get(Calendar.YEAR);
    }

    private void setAnioIngreso(int p_anioIngreso) {
        Calendar c1 = new GregorianCalendar();
        c1.set(Calendar.YEAR, p_anioIngreso);
        this.setFechaIngreso(c1);
    }

    public Calendar getFechaIngreso(){
        return this.fechaIngreso;
    }

    private void setFechaIngreso(Calendar p_fechaIngreso){
        this.fechaIngreso = p_fechaIngreso;
    }
}
