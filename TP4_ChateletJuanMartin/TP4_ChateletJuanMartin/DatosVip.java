import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;
import java.io.*;

/**
 * Write a description of class DatosVip here.
 *
 * @author Juan Chatelet
 * @version 19/09/19
 */
public class DatosVip{
    public static void main(String[] args) {
        long cuil;
        String apellido;
        String nombre;
        double sueldo;
        double sueldoNeto;
        int dia, mes, anio;
        Calendar fechaIngreso;
        ArrayList<Empleado> empleadosVip = new ArrayList<>();
        try {
            // Creando un objeto de tipo archivo secuencial para leer
            FileInputStream archiFIS=new FileInputStream("/home/juanchate/archivosPOO/Empleado.dat");
            DataInputStream archiDIS = new DataInputStream(archiFIS);
            /*
            LU = archiDIS.readInt();
            nombre = archiDIS.readUTF();
            edad = archiDIS.readInt();
            */
            while (archiDIS.available()>0){ // mientras haya bytes para leer
                //Se leen los datos en el archivo Empleado.dat
                cuil = archiDIS.readLong();
                apellido = archiDIS.readUTF();
                nombre = archiDIS.readUTF();
                sueldo = archiDIS.readDouble();
                sueldoNeto = archiDIS.readDouble();
                dia = archiDIS.readInt();
                mes = archiDIS.readInt();
                anio = archiDIS.readInt();
                fechaIngreso = new GregorianCalendar(anio, mes, dia);
                Empleado tempEmpleado = new Empleado(cuil, apellido, nombre , sueldo, fechaIngreso);
                empleadosVip.add(tempEmpleado);
                System.out.println("asdf");
            }
            // Se cierra el sistema de archivos logico de escritura  
            archiDIS.close();
            archiFIS.close();
        } // Cierra el try
        catch(FileNotFoundException fnfe) {
            System.out.println("Archivo no encontrado");
        }
        catch(IOException ioe){
            System.out.println("Error al leer");
        }
            // Se itera sobre cada uno de los empleados
            try {
                // Creando un objeto de tipo archivo secuencial para grabar
                FileOutputStream archiFOS = new FileOutputStream("/home/juanchate/archivosPOO/EmpleadoVIP.dat", true);
                DataOutputStream archiDOS = new DataOutputStream(archiFOS);
                for (Empleado emp : empleadosVip) {
                    // Si la antiguedad del empleado es mayor a 10 se graba en EmpleadoVIP.dat
                    if (emp.antiguedad() > 10) {
                        System.out.println("asdf");
                            //Se graban los datos en el archivo EmpleadoVIP.dat
                            archiDOS.writeLong(emp.getCuil());
                            archiDOS.writeUTF(emp.getApellido());
                            archiDOS.writeUTF(emp.getNombre());
                            archiDOS.writeDouble(emp.getSueldoBasico());
                            archiDOS.writeDouble(emp.sueldoNeto());
                            archiDOS.writeInt(emp.getFechaIngreso().get(Calendar.DAY_OF_MONTH));
                            archiDOS.writeInt(emp.getFechaIngreso().get(Calendar.MONTH));
                            archiDOS.writeInt(emp.getFechaIngreso().get(Calendar.YEAR)); 
                    }
                }
                archiDOS.close();
                archiFOS.close();
            } // Cierra el try
            catch(FileNotFoundException fnfe) {
                System.out.println("Archivo no encontradoo");
            }
            catch(IOException ioe){
                System.out.println("Error al grabar");
            }  
            // Se cierra el sistema de archivos logico de escritura   
    } // Cierra main
} // Cierra clase
